const GET_DATA_ERROR_TEXT = 'Błąd pobierania danych o paragonie';
const BILL_WAS_PRINTED_SUCCESS_TEXT = 'Paragon został wydrukowany';
const BILL_WAS_PRINTED_ERROR_TEXT = 'Błąd wydruku paragonu';
const SAVE_RECEIPT_ERROR_TEXT =
  'Błąd zapisu informacji o wydrukowanym paragonie ';

class GoingPrintServer {
  state = {
    apiUrl: null,
    transactionId: null,
    authToken: null,
    printSuccessUrl: null,
    printerUrl: null,
  };

  static snakeToCamel(str) {
    return str.toLowerCase().replace(/([-_]\w)/g, (g) => g[1].toUpperCase());
  }

  run() {
    this.applyCurrentActionsFromQueue();
    this.getReceipt();
  }

  applyCurrentActionsFromQueue() {
    const actions = window.goingPrintServerQueue || [];

    actions.map((action) => {
      const { type, payload } = action;

      if (type) {
        const actionName = GoingPrintServer.snakeToCamel(type);

        if (
          Object.prototype.hasOwnProperty.call(
            GoingPrintServer.prototype,
            actionName
          )
        ) {
          this[actionName](payload);
        }
      }
    });
  }

  getReceipt() {
    const http = new XMLHttpRequest();

    const { apiUrl, transactionId } = this.state;

    http.open('GET', apiUrl, true);
    http.setRequestHeader('Content-type', 'application/json');

    if (this.state.authToken) {
      http.setRequestHeader('Authorization', 'Bearer ' + this.state.authToken);
    }

    http.onreadystatechange = () => {
      if (http.readyState === 4) {
        if (http.status === 200) {
          // const printData = {
          //   lines: [
          //     {
          //       na: 'Towar 1 Mariusz 1234556677890123456321121  ',
          //       il: 1,
          //       vt: 1,
          //       pr: 12356,
          //     },
          //   ],
          //   summary: {
          //     to: 12356,
          //   },
          //   extralines: [
          //     {
          //       id: 2,
          //       na: '987',
          //     },
          //   ],
          // };

          const { printData } = http.response;
          const { printerUrl } = this.state;

          if (!printerUrl || !printData) return null;

          this.printReceipt(printerUrl, printData);
        } else {
          alert(GET_DATA_ERROR_TEXT);
        }
      }
    };
    http.send();
  }

  printReceipt(url, printData) {
    const http = new XMLHttpRequest();

    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/json');

    http.onreadystatechange = () => {
      if (http.readyState === 4) {
        if (http.status === 200) {
          alert(BILL_WAS_PRINTED_SUCCESS_TEXT);
          this.saveDataWhenReceiptWasPrinted();
        } else {
          alert(BILL_WAS_PRINTED_ERROR_TEXT);
        }
      }
    };
    http.send(JSON.stringify(printData));
  }

  saveDataWhenReceiptWasPrinted() {
    const http = new XMLHttpRequest();
    const { printSuccessUrl } = this.state;
    http.open('POST', printSuccessUrl, true);
    http.setRequestHeader('Content-type', 'application/json');

    if (this.state.authToken) {
      http.setRequestHeader('Authorization', 'Bearer ' + this.state.authToken);
    }

    http.onreadystatechange = () => {
      if (http.readyState === 4) {
        if (http.status === 200) {
          alert('Informacja została zapisana w api');
        } else {
          const response = JSON.parse(http.response);

          if (response && response.message) {
            alert(SAVE_RECEIPT_ERROR_TEXT + response.message);
          } else {
            alert(SAVE_RECEIPT_ERROR_TEXT);
          }
        }
      }
    };
    http.send(JSON.stringify());
  }

  setApiUrl(apiUrl) {
    this.setState({
      apiUrl,
    });
  }

  setPrintSuccessUrl(printSuccessUrl) {
    this.setState({
      printSuccessUrl,
    });
  }

  setAuthorization(authToken) {
    this.setState({
      authToken,
    });
  }

  setTransactionId(transactionId) {
    this.setState({
      transactionId,
    });
  }

  setPrinterUrl(printerUrl) {
    console.log('ustawiam printer url', printerUrl);
    this.setState({
      printerUrl,
    });
  }

  setState(stateFragment) {
    this.state = Object.assign(this.state, stateFragment);
  }
}

window.GoingPrintServerVersion = '1.0.0';
window.GoingPrintServerService =
  window.GoingPrintServerService || new GoingPrintServer();
window.GoingPrintServerService.run();
