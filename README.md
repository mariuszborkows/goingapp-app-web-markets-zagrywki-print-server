# goingapp-app-web-markets-zagrywki-script

## Jak osadzamy skrypt?

W sekcji head dodajemy:

```html
<script>
   (function (G, o, i, n, g) {
       G.going = G.going || function () {[].push.apply((G.goingQ = G.goingQ || []), arguments)};
       G.goingSettings = {gv: 1, gid: 1};
       n = o.getElementsByTagName('head')[0];
       g = o.createElement('script');
       g.async = 1;
       g.src = i + '?gid=' + G.goingSettings.gid + '&gv=' + G.goingSettings.gv;
       n.appendChild(g);
   })(window, document, 'script.js');
</script>
```

Ważne, aby poprawnie podać **gid** - jest to identyfikator, za pomocą którego rozpoznamy klienta.
Na ten moment jest to nieistotne. Natomiast **gv** oznacza wersję skryptu, ktora powinna się załadować.
Trzeci parametr wywołania to `'script.js'`, co na stagu będzie oznaczać jaki rodzaj skryptu
chcemy wczytać. Na ten moment będzie jedna wersja - produkcyjna, ale nic nie stoi na przeszkodzie,
żeby to zmienić jak zajdzie potrzeba na wersje dedykowane.

## Jak skonfigurować skrypt?

Aby skonfigurować skrypt musimy wykonać na nim kilka akcji. Poniżej znajduje się lista akcji oraz przykład:

* Ustawianie pokoju *
```
 type: 'SET_ROOM',
 payload: 'identyfikator pokoju'
```

* Ustawianie elementu, w którym pojawi się iframe (nie zastapi!) *
```
 type: 'SET_PARENT',
 payload: 'identyfikator rodzica'
```

* Ustawianie transakcji
```
 type: 'SET_TRANSACTION',
 payload: 'identyfikator transakcji'
```

* Ustawianie tokenu
```
 type: 'SET_TOKEN',
 payload: 'token'
```

* Ustawianie automatycznego pobrania transakcji z url
```
 type: 'GET_TRANSACTION_FROM_URL',
 payload: 'nazwa parametru'
```

**Ważne jest tutaj, że kolejność akcji ma wpływ na ilość zmian `src` dla iframe.**

Skrypt kiedy wykryje, że istnieją takie parametry jak roomId i parentId stwierdzi, że ma
wystarczające informacje, by budować iframe. Jeżeli po tym nastąpi ustawienie identyfikatora
transakcji, skrypt będzie zmuszony przeładować ramkę. Nie ma obaw, że dane nie zostaną
przekazane - to nastąpi zawsze gdy będzie ustawiona para, ale podając dane w odpowiedniej kolejności możemy mieć sytuację,
w ktorej skrypt załaduje ramkę dokładnie raz :).

## Przykład

Jeżeli mamy w sekcji **head** osadzony skrypt, w dowolnym miejscu na stronie możemy wykonać w ramach `script`:

```javascript
going({
   type: 'SET_ROOM',
   payload: 'pokoj-bez-nazwy'
}, {
   type: 'SET_TRANSACTION',
   payload: 'id-transakcji'
}, {
   type: 'SET_PARENT',
   payload: 'parentId'
});
```

## Asynchroniczność

Jako, że skrypt może ładować się różnie w zależności od łącza internetowego, musimy mieć pewność,
że wszystkie dane zostaną przesłane prawidłowo. W tym celu używamy  **ping-ponga** by się wymienić uściskami dłoni.

W pierwszej kolejności to dziecko musi wysłać akcję *PING*. Jeżeli rodzic ją odbierze,
to wyśle wszystkie akcje, które są aktualnie w kolejce, w szczególności wyśle akcje
ustawiające pokój, transakcję, czy token. Na końcu wyśle akcję *PONG*. Oznaczać to będzie dla dziecka,
że nie ma już zaległości. (Dziecko nie musi na nie reagować, bo one są także wysyłane w urlu, ale może, gdyby jednak urla nie respektowało).
Od momentu wysłania przez rodzica informacji *PONG*, mamy pewność, że komunikacja została nawiązana
i może być prowadzona w czasie rzeczywistym, stąd każda akcja z rodzica idzie bezpośrednio do dziecka.
Dziecko może w dowolnej chwili wysyłać akcje rodzicowi, bo skoro dziecko zostało zamontowane, to
oznacza, że rodzic już nasłuchuje.

## Akcje odbierane przez rodzica

Dziecko może komunikować się z rodzicem. Oto typy akcji, które może rzucić:

* Ustawienie ramki na wskazaną wysokość (głównie będzie to wysokość dziecka)
```
 type: 'RESIZE',
 payload: 'wysokość razem z jednostką px'
```

* Wykonanie redirecta
```
 type: 'REDIRECT',
 payload: 'url, pod który ma przekierować'
```

* Scrollowanie do góry strony
```
 type: 'SCROLL_TO_TOP',
```

* Akcja informująca, że dziecko jest gotowe by nawiązać komunikację
```
 type: 'PING',
```

## Konstrukcja obiektu wysyłanego do rodzica:

Ogólny schemat wygląda następująco:

```
 type: 'GOING',
 action: {
    type: 'TYP_AKCJI',
    payload: object | string | number | undefined
 }
```

Event, który wygląda jak powyżej powinien zostać przekazany przez **postMessage**.

## Zbudowanie skryptu

Aby zbudować skrypt, który znajdować się będzie w katalogu `lib` należy wykonać:

1. Zainstalować moduły
```
npm install
```

2. Zbudować zminifikowaną wersje
```
npm run start
```
